DROP DATABASE IF EXISTS QLSV;

CREATE DATABASE QLSV;

USE QLSV;

CREATE TABLE SINHVIEN(
    MaSV varchar(6) PRIMARY KEY,
    HoSV varchar(30),  
    TenSV varchar(15),
    GioiTinh char(1),
    NgaySinh datetime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong int
)


CREATE TABLE DMKHOA(
    MaKH varchar(6) PRIMARY KEY,
    TenKhoa varchar(30)
);